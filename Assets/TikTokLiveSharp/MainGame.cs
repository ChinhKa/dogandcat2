﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TikTokLiveSharp.Models;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using DG.Tweening;
public class MainGame : MonoBehaviour
{
    public static MainGame instance;
    private Dictionary<int, Dictionary<User, int>> listUser;
    public Character CatCharacter;
    public Character DogCharacter;

    [Header("TOTAL CORE:")]
    private int scoreDog;
    protected int scoreDogTotal;
    private int scoreCat;
    protected int scoreCatTotal;
    protected Dictionary<User, int> sortedList1, sortedList2;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        listUser = new Dictionary<int, Dictionary<User, int>>();
        listUser.Add(1, new Dictionary<User, int>());
        listUser.Add(2, new Dictionary<User, int>());
        TiktokLiveManager.instance.id = PlayerPrefs.GetString("RoomID");
        TiktokLiveManager.instance.Init();
    }

    private void OnApplicationQuit()
    {
        TiktokLiveManager.instance.Stop();
    }

    private void Update()
    {    
        if (GameManager.ins.startSeesion)
        {
            while (TiktokLiveManager.instance.mesQueue.Count > 0)
            {
                var mesData = TiktokLiveManager.instance.mesQueue.Dequeue();

                if (mesData is WebcastChatMessage chatMsg)
                {
                    if (!listUser[1].Any(kv => kv.Key.userId == chatMsg.User.userId) &&
                    !listUser[2].Any(kv => kv.Key.userId == chatMsg.User.userId))
                    {
                        if (chatMsg.Comment == "1")
                        {
                            DogCharacter.IncreaseVotes();
                            listUser[1].Add(chatMsg.User, 0);
                        }
                        else if (chatMsg.Comment == "2")
                        {
                            CatCharacter.IncreaseVotes();
                            listUser[2].Add(chatMsg.User, 0);
                        }
                    }
                    else
                    {
                        if (sortedList1.Any())
                        {
                            var firstUserKeyValuePair = sortedList1.First();  
                            User firstUser = firstUserKeyValuePair.Key;  
                            int firstUserValue = firstUserKeyValuePair.Value;  

                            if (firstUser.userId == chatMsg.User.userId)
                            {
                                UIManager.ins.ShowCommentKingDog(chatMsg.Comment);
                            }
                        }

                        if (sortedList2.Any())
                        {
                            var firstUserKeyValuePair = sortedList2.First();  
                            User firstUser = firstUserKeyValuePair.Key;  
                            int firstUserValue = firstUserKeyValuePair.Value; 

                            if (firstUser.userId == chatMsg.User.userId)
                            {
                                UIManager.ins.ShowCommentKingCat(chatMsg.Comment);
                            }
                        }
                    }
                }
                else if (mesData is WebcastGiftMessage giftMsg)
                {
                    if (!listUser[1].Any(kv => kv.Key.userId == giftMsg.User.userId) &&
                   !listUser[2].Any(kv => kv.Key.userId == giftMsg.User.userId))
                    {
                        if(giftMsg.giftDetails.diamondCount <= 1)
                        {
                            listUser[1].Add(giftMsg.User,0);//Dg
                        }
                        else
                        {
                            listUser[2].Add(giftMsg.User,0);//Cat
                        }
                    }


                    if (listUser[1].ContainsKey(giftMsg.User) && giftMsg.giftDetails.diamondCount <= 1)
                    {
                        listUser[1][giftMsg.User] += giftMsg.giftDetails.diamondCount * giftMsg.repeatCount;
                        int times = giftMsg.giftDetails.diamondCount * giftMsg.repeatCount;
                        scoreDog += times;
                        scoreDogTotal += times;
                        DogCharacter.UpdateScore(scoreDog);
                        DogCharacter.UpdateScoreTotal(scoreDogTotal);
                    }
                    else if (listUser[2].ContainsKey(giftMsg.User))
                    {
                        listUser[2][giftMsg.User] += giftMsg.giftDetails.diamondCount * giftMsg.repeatCount;
                        int times = giftMsg.giftDetails.diamondCount * giftMsg.repeatCount;
                        scoreCat += times;
                        scoreCatTotal += times;
                        CatCharacter.UpdateScore(scoreCat);
                        CatCharacter.UpdateScoreTotal(scoreCatTotal);
                    }
                }
            }

            #region List Processing

            //Sort
            sortedList1 = listUser[1].OrderByDescending(kv => kv.Value).ToDictionary(kv => kv.Key, kv => kv.Value);//Dog
            sortedList2 = listUser[2].OrderByDescending(kv => kv.Value).ToDictionary(kv => kv.Key, kv => kv.Value);//Cat

            //Display
            int a = 0, b = 0;
            if(sortedList1.Count > 0)//Dog
            {

                foreach (var innerKeyValuePair in sortedList1)
                {
                    User user = innerKeyValuePair.Key;
                    int value = innerKeyValuePair.Value;

                    string nickname = user.Nickname;
                    UIManager.ins.dogTops[a].text = nickname;
                    if(a == 4)
                    {
                        return;
                    }
                    else
                    {
                        a++;
                    }
                }
            }
            if(sortedList2.Count > 0)//Cat
            {
                foreach (var innerKeyValuePair in sortedList2)
                {
                    User user = innerKeyValuePair.Key;
                    int value = innerKeyValuePair.Value;

                    string nickname = user.Nickname;
                    UIManager.ins.catTops[b].text = nickname;                
                    if(b == 4)
                    {
                        return;
                    }
                    else
                    {
                        b++;
                    }
                }
            }

            #endregion


            AttackProcessingAfterTenSeconds();
        }
    }

    private void AttackProcessingAfterTenSeconds()
    {
        if (GameManager.ins.finishVoteTime)
        {
            if(scoreCat != scoreDog)
            {
                SoundManager.ins.PlaySound(SoundManager.ins.shoutClip);
                if (scoreCat > scoreDog)
                {
                    AttackHandle(scoreCat, CatCharacter.GetComponent<CharacterAnim>());
                }
                else if (scoreCat < scoreDog)
                {
                    AttackHandle(scoreDog, DogCharacter.GetComponent<CharacterAnim>());
                }
            }

            ClearScore();
            GameManager.ins.finishVoteTime = false;
        }
    }

    public virtual void AttackHandle(int core, CharacterAnim anim)
    {
        if (core >= 0)
        {
            anim.SetPushReady(true);
        }
    }

    public void ResetTotal()
    {
        listUser[1].Clear();
        listUser[2].Clear();
        ClearScore();
        ClearScoreTotal();
    }

    public void ClearScoreTotal()
    {
        scoreCatTotal = scoreDogTotal = 0;
        CatCharacter.UpdateScoreTotal(0);
        DogCharacter.UpdateScoreTotal(0);
    }

    public void ClearScore()
    {
        scoreCat = scoreDog = 0;
        CatCharacter.UpdateScore(0);
        DogCharacter.UpdateScore(0);
    }

    public void AddCatPoint()
    {
        scoreCat += 500;
        CatCharacter.UpdateScore(scoreCat);
    }

    public void AddDogPoint()
    {
        scoreDog += 500;
        DogCharacter.UpdateScore(scoreDog);
    }
}
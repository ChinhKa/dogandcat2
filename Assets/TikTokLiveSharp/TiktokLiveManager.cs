/// Basic profile picture handling example.
/// - @sebheron 2022

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using TikTokLiveSharp.Client;
using TikTokLiveSharp.Models;
using ProtoBuf;

public class TiktokLiveManager: MonoBehaviour
{
    public static TiktokLiveManager instance;
    /// <summary>
    /// The uniqueId for the stream.
    /// </summary>
    [HideInInspector] public string id;

    /// <summary>
    /// The TikTokLiveClient, initalised on Start.
    /// </summary>
    private static TikTokLiveClient _client;

    /// <summary>
    /// Queue of mes
    /// </summary>
    public Queue<IExtensible> mesQueue;

    private void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// Start method. Initalises 
    /// </summary>
    /// 
    public void Init()
    {
        _client = new TikTokLiveClient(id);
        mesQueue = new Queue<IExtensible>();
        // Subscribe to the OnCommentRecieved event.
        _client.OnCommentRecieved += Client_OnCommentRecieved;
        _client.OnGiftRecieved += Client_OnGiftRecieved;

        //Try catch here to log any errors that occur while starting.
        try
        {
            // Start the client.
            // In Unity we cannot use the Run method as this will block the main thread.
            _client.Start();
        }
        catch (System.Exception e)
        {
            Debug.LogException(e);
        }
    }

    /// <summary>
    /// On Destroy method.
    /// Required as seperate threads continuing running outside of play mode in Unity.
    /// Without this Client_OnCommentRecieved would continue to be called.
    /// </summary>
    public void Stop()
    {
        _client.Stop();
        // Unsubscribe from the OnCommentRecieved event.
        _client.OnCommentRecieved -= Client_OnCommentRecieved;
        _client.OnGiftRecieved -= Client_OnGiftRecieved;
    }

    /// <summary>
    /// Comment received method.
    /// </summary>
    void Client_OnCommentRecieved(object sender, WebcastChatMessage e)
    {
        Debug.Log(e.Comment);
        // We need to lock the profile pictures queue as we're not currently working on the main thread.
        lock (mesQueue)
        {
            mesQueue.Enqueue(e);
        }
    }

    /// <summary>
    /// Comment received method.
    /// </summary>
    void Client_OnGiftRecieved(object sender, WebcastGiftMessage e)
    {
        Debug.LogError(e.giftDetails.diamondCount + "+" + e.repeatCount + "+" + e.giftDetails.giftName);
        // We need to lock the profile pictures queue as we're not currently working on the main thread.
        lock (mesQueue)
        {
            mesQueue.Enqueue(e);
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class UIMain : MonoBehaviour
{
    public static UIMain ins;

    [Header("UI")]
    public GameObject loginForm;
    public GameObject registerForm;
    public GameObject roomUI;
    public GameObject homeUI;
    public GameObject messUI;

    [Space]
    [Header("BUTTON:")]
    [Header("JOIN ROOM:")]
    public Button btnJoinRoom;
    [Header("LOGIN:")]
    public Button btnLogin;
    public Button btnLogin_Form;
    [Header("REGISTER:")]
    public Button btnRegister;
    public Button btnRegister_Form;

    public Button btnCloseMess;

    [Space]
    [Header("OTHER:")]
    public TMP_InputField txtRoomID;
    public TextMeshProUGUI txtMess;
    public Transform iconFight;
    public Transform title;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        ClickEvent();
        iconFight.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.3f).SetLoops(-1, LoopType.Yoyo);
        title.localScale = Vector3.zero;
        title.DOScale(Vector3.one, 1f).SetEase(Ease.InOutBack);
        homeUI.SetActive(true);
        messUI.SetActive(false);
        ActiveUI(loginForm);
    }

    private void ClickEvent()
    {
        btnLogin_Form.onClick.AddListener(() =>
        {
            ActiveUI(loginForm);
        });

        btnRegister_Form.onClick.AddListener(() =>
        {
            ActiveUI(registerForm);
        });

        btnLogin.onClick.AddListener(() =>
        {
            NetworkManager.ins.Login();
        });

        btnRegister.onClick.AddListener(() =>
        {
            NetworkManager.ins.Register();
        });

        btnCloseMess.onClick.AddListener(() =>
        {
            messUI.SetActive(false);
        });

        btnJoinRoom.onClick.AddListener(() =>
        {
            btnJoinRoom.transform.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 0.1f).OnComplete(() =>
            {
                btnJoinRoom.transform.DOScale(Vector3.one, 0.1f).OnComplete(() =>
                {
                    if (!string.IsNullOrEmpty(txtRoomID.text))
                    {
                        PlayerPrefs.SetString(Key.RoomID.ToString(), txtRoomID.text);
                        PlayerPrefs.Save();
                        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                    }
                    else
                    {
                        // mess.SetActive(true);
                    }
                });
            });
        });
    }

    public void ShowMess(string mess)
    {
        messUI.SetActive(true);
        txtMess.text = mess;
    }

    public void ShowRoom()
    {
        homeUI.SetActive(false);
        ActiveUI(roomUI);
    }

    private void ActiveUI(GameObject ui)
    {
        loginForm.SetActive(false);
        registerForm.SetActive(false);
        roomUI.SetActive(false);

        if (ui != null)
        {
            ui.SetActive(true);
        }
    }
}

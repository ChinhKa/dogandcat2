using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "New Data")]
public class Data : ScriptableObject
{
    [Header("HARD DATA:")]

    [Header("TIME LINE:")]
    public float sessionTime;
    public float voteTime;
    public float nextSessionTime;
    public float nextDaySessionTime;
    public float coolDownFight;

    [Header("LANDMARK POINT:")]
    public float landmarkPoint_Skill1;
    public float landmarkPoint_Skill2;
    public float landmarkPoint_Skill3;
    public float landmarkPoint_Skill4;
    public float landmarkPoint_Skill5;

    [Space]
    [Header("SOFT DATA:")]
    public CharacterDB characterDB;
}

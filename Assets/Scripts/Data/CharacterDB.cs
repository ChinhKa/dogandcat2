using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DBCharacter", menuName = "New DB Character")]
public class CharacterDB : ScriptableObject
{
    public List<CharacterInfo> listCharacter = new List<CharacterInfo>();
}

[System.Serializable]
public class CharacterInfo
{
    [Header("BASE INDEX:")]
    public IDCharacter ID;
    public string name;
    public int baseAttack;
    public int baseHP;
    public int moveSpeed;
    public int attackSpeed;

    [Space]
    [Header("SOUND:")]
    public AudioClip attackSound;
    public AudioClip skill1Sound;
    public AudioClip skill2Sound;
    public AudioClip skill3Sound;
    public AudioClip skill4Sound;
    public AudioClip skill5Sound;

    [Space]
    [Header("UI:")]
    public Sprite icon;
}
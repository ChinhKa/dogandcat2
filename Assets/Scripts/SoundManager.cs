using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager ins;

    [Header("AUDIO SOURCE:")]
    public AudioSource musicSound_AudioSource;
    public AudioSource effectSound_AudioSource;

    [Space]
    [Header("AUDIO CLIP:")]
    public AudioClip BGClip;
    public AudioClip[] clapClips;
    public AudioClip shoutClip;
    public AudioClip punchClip;
    public AudioClip countDownFightClip;
    public AudioClip finishSound;

    private float countDownClap;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        //PlayMusicSound(true);
    }

    private void Update()
    {
        if (GameManager.ins.isLoaded)
        {
            if(countDownClap <= 0)
            {
                int rand = Random.Range(0,2);
                PlaySound(clapClips[rand]);
                countDownClap = 2;
            }
            else
            {
                countDownClap -= Time.deltaTime;
            }
        }
    }

    public void PlayMusicSound(bool isLoop)
    {
        musicSound_AudioSource.clip = BGClip;
        musicSound_AudioSource.Play();
        musicSound_AudioSource.loop = isLoop;
    }

    public void PlaySound(AudioClip clip)
    {
        effectSound_AudioSource.PlayOneShot(clip);
    }
}

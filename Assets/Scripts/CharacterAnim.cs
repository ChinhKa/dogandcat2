using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnim : MonoBehaviour
{
    public Action OnAnimComplete;

    [SerializeField] private Animator animator;

    private bool isPushReady;

    public void TriggerAnim(string anim)
    {
        animator.SetTrigger(anim);
    }

    public void ResetAnim() => animator.Rebind();

    #region  AttackReady(get,set)
    public void SetPushReady(bool rs)
    {
        isPushReady = rs;
    }

    public bool GetPushReady() => isPushReady;
    #endregion
}

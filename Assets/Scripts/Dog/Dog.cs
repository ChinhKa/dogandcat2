using System.Collections;
using UnityEngine;
using DG.Tweening;

public class Dog : Character
{
    public override void LoadData()
    {
        base.LoadData();
        target = MainGame.instance.CatCharacter.transform;
    }

    public override void Rotate()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
    }

    public override void ShowWins()
    {
        winsText.text = GameManager.ins.dogWins.ToString();
    }    

    public override void BePushBack()
    {
        base.BePushBack();
        Vector3 backwardDirection = transform.forward;
        GetComponent<Rigidbody>().AddForce((backwardDirection * forcePush), ForceMode.Force);
        Debug.Log("Bi meo day");
    }
}

﻿using UnityEngine;

public class AnimEvent : MonoBehaviour
{
    public Character character;
    public void Push()
    {
        character.Push();
        SoundManager.ins.PlaySound(character.attackSound);
    }

    public void EnableAllAttack()
    {
        GameManager.ins.enableAttackAll = true; 
    }
}

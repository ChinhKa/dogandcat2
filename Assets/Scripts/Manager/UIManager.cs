using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.Collections.Generic;
using System.Collections;

public class UIManager : MonoBehaviour
{
    public static UIManager ins;

    [Header("UI:")]
    public GameObject finishSession_UI;
    public GameObject fightingUI;
    public GameObject finishDaySession_UI;
    public GameObject loading_UI;

    [Space]
    [Header("TXT:")]
    public TextMeshProUGUI txtWinner;
    public TextMeshProUGUI txtSessionTime;
    public TextMeshProUGUI txtNextSessionTime;
    public TextMeshProUGUI txtATKTime;
    public TextMeshProUGUI txtSummaryTime;
    public TextMeshProUGUI txtNextDaySessionTime;
    public TextMeshProUGUI txtDogCat_WinsTotal;

    public TextMeshProUGUI txtCountDown_Fight;
    public Transform txtFightEff;

    [Space]
    [Header("DOG:")]
    public GameObject commentKingDog;
    public Vector3 baseSizeCmtDog;
    public ParticleSystem sprayMoneyDogEff;
    public List<Text> dogTops = new List<Text>();

    [Header("CAT:")]
    public GameObject commentKingCat;
    public Vector3 baseSizeCmtCat;
    public ParticleSystem sprayMoneyCatEff;
    public List<Text> catTops = new List<Text>();

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        ActiveUI(fightingUI);
        baseSizeCmtCat = commentKingCat.transform.localScale;
        baseSizeCmtDog = commentKingDog.transform.localScale;
        ShowFightEff();
        StartCoroutine(Loading());
    }

    public void HiddenSprayMoney()
    {
        commentKingCat.transform.LookAt(commentKingCat.transform.position + Camera.main.transform.forward);
        commentKingDog.transform.LookAt(commentKingDog.transform.position + Camera.main.transform.forward);
        sprayMoneyDogEff.gameObject.SetActive(false);
        sprayMoneyCatEff.gameObject.SetActive(false);
    }

    public void ShowCommentKingDog(string cmt) => StartCoroutine(CommentKingDog(cmt));
    public void ShowCommentKingCat(string cmt) => StartCoroutine(CommentKingCat(cmt));


    public IEnumerator CommentKingDog(string cmt)
    {
        commentKingDog.SetActive(true);
        commentKingDog.transform.localScale = Vector3.zero;
        commentKingDog.transform.DOScale(baseSizeCmtDog, 0.5f);
        commentKingDog.transform.GetChild(0).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = cmt;
        sprayMoneyDogEff.gameObject.SetActive(true);
        sprayMoneyDogEff.Play();
        yield return new WaitForSeconds(3);
        commentKingDog.transform.DOScale(Vector3.zero, 0.5f).OnComplete(() =>
        {
            commentKingDog.SetActive(false);
        });
        sprayMoneyDogEff.gameObject.SetActive(false);
    }

    public IEnumerator CommentKingCat(string cmt)
    {
        commentKingCat.SetActive(true);
        commentKingCat.transform.localScale = Vector3.zero;
        commentKingCat.transform.DOScale(baseSizeCmtCat, 0.5f);
        commentKingCat.transform.GetChild(0).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = cmt;
        sprayMoneyCatEff.gameObject.SetActive(true);
        sprayMoneyCatEff.Play();
        yield return new WaitForSeconds(3);
        commentKingCat.transform.DOScale(Vector3.zero, 0.5f).OnComplete(() =>
        {
            commentKingCat.SetActive(false);
        });
        sprayMoneyCatEff.gameObject.SetActive(false);
    }

    public IEnumerator Loading()
    {
        loading_UI.SetActive(true);
        GameManager.ins.isLoaded = false;
        yield return new WaitForSeconds(3f);
        loading_UI.SetActive(false);
        GameManager.ins.isLoaded = true;
    }

    public void ShowFinishSessionUI() => ActiveUI(finishSession_UI);
    public void ShowFinishDaySessionUI()
    {
        txtDogCat_WinsTotal.text = "DOG: " + GameManager.ins.dogWins.ToString() + " - CAT: " + GameManager.ins.catWins.ToString();
        ActiveUI(finishDaySession_UI);
    }
    public void ActiveUI(GameObject ui)
    {
        fightingUI.SetActive(false);
        finishSession_UI.SetActive(false);
        finishDaySession_UI.SetActive(false);

        if (ui != null)
        {
            ui.SetActive(true);
        }
    }
    public void ShowFightEff()
    {
        ShowCommentKingDog("1");
        ShowCommentKingCat("2");
        txtFightEff.localScale = Vector3.zero;
        txtFightEff.DOScale(Vector3.one, 1f);
    }

    public void ClearTops()
    {
        foreach (var o in dogTops)
        {
            o.text = "";
        }

        foreach (var o in catTops)
        {
            o.text = "";
        }
    }
}

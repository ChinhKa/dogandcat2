﻿using DG.Tweening;
using System;
using UnityEngine;
public class GameManager : MonoBehaviour
{
    public static GameManager ins;

    [Space]
    [Header("EVENT GAME:")]
    [HideInInspector] public bool finishSeesion;
    [HideInInspector] public bool startSeesion;
    [HideInInspector] public bool finishVoteTime;
    [HideInInspector] public bool finishDaySession;
    [HideInInspector] public bool isLoaded;
    /*[HideInInspector] */public bool enableAttackAll;
    private bool playedFightSound;
    public Action OnStartAgainSeesion;
    public Action OnFinishSeesion;
    public Action OnFinishDay;

    [Space]
    [Header("COOL DOWN:")]
    private float countDown_SessionTime;
    private float countDown_NextSessionTime;
    private float countDown_VoteTime;
    private float countDown_NextDaySessionTime;
    public float countDown_Fight;

    [Space]
    [Header("SCORE OF THE MATCH")]
    public int dogWins;
    public int catWins;

    private void Awake()
    {
        ins = this;
    }

    private void Start()
    {
        SetUpData();
        OnFinishSeesion += HandleFinishedSeesion;
        OnStartAgainSeesion += HandleStartAgainSeesion;
        OnFinishDay += HandleFinishDay;
    }

    public void SetUpData()
    {
        playedFightSound = false;
        finishSeesion = false;
        finishVoteTime = false;
        finishDaySession = false;
        startSeesion = false;
        enableAttackAll = true;
        countDown_SessionTime = DataManager.ins.data.sessionTime;
        countDown_NextSessionTime = DataManager.ins.data.nextSessionTime;
        countDown_NextDaySessionTime = DataManager.ins.data.nextDaySessionTime;
        countDown_VoteTime = DataManager.ins.data.voteTime;
        countDown_Fight = DataManager.ins.data.coolDownFight;
        MainGame.instance.ResetTotal();
        UIManager.ins.ClearTops();
    }

    private void Update()
    {
        if (isLoaded)
        {
            if (!startSeesion)
            {
                FightTime();
            }

            if (!finishDaySession)
            {
                if (!finishSeesion)
                {
                    SessionTime();
                    VoteTime();
                    SummaryTime();
                }
                else
                {
                    NextSessionTime();
                }
            }
            else
            {
                NextDaySessionTime();
            }
        }
    }

    private void VoteTime()
    {
        if (countDown_VoteTime <= 0)
        {
            finishVoteTime = true;
            countDown_VoteTime = DataManager.ins.data.voteTime;
            UIManager.ins.txtATKTime.text = "00:00";
        }
        else
        {
            countDown_VoteTime -= Time.deltaTime;
            UIManager.ins.txtATKTime.text = Mathf.Floor(countDown_VoteTime / 60).ToString("00") + ":" + (countDown_VoteTime % 60).ToString("00");
        }
    }

    private DateTime currentDateTime;
    private DateTime endOfDay;

    private void SummaryTime()
    {
        currentDateTime = DateTime.Now;

        endOfDay = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, 23, 59, 59);

        TimeSpan timeRemaining = endOfDay - currentDateTime;

        if (timeRemaining.Hours <= 0 && timeRemaining.Minutes <= 0 && timeRemaining.Seconds <= 0)
        {
            finishDaySession = true;
            OnFinishDay?.Invoke();
            UIManager.ins.txtSummaryTime.text = "00:00:00";
        }
        else
        {
            UIManager.ins.txtSummaryTime.text = timeRemaining.Hours + ":" + timeRemaining.Minutes + ":" + timeRemaining.Seconds;
        }
    }

    private void SessionTime()
    {
        if (countDown_SessionTime <= 0)
        {
            UIManager.ins.txtSessionTime.text = "00:00";
            OnFinishSeesion?.Invoke();
        }
        else
        {
            countDown_SessionTime -= Time.deltaTime;
            UIManager.ins.txtSessionTime.text = Mathf.Floor(countDown_SessionTime / 60).ToString("00") + ":" + (countDown_SessionTime % 60).ToString("00");
        }
    }

    private void NextSessionTime()
    {
        countDown_NextSessionTime -= Time.deltaTime;
        if (countDown_NextSessionTime <= 0)
        {
            OnStartAgainSeesion?.Invoke();
        }
        else
        {
            UIManager.ins.txtNextSessionTime.text = Math.Round(countDown_NextSessionTime) + "s";
        }
    }

    private void FightTime()
    {
        countDown_Fight -= Time.deltaTime;
        if (countDown_Fight <= 0)
        {
            UIManager.ins.txtCountDown_Fight.text = "0s";
            UIManager.ins.txtFightEff.DOScale(Vector3.zero, 1f).OnComplete(() =>
            {
                startSeesion = true;
            });
        }
        else
        {
            if (!playedFightSound)
            {
                SoundManager.ins.PlaySound(SoundManager.ins.countDownFightClip);
                playedFightSound = true;
            }
            UIManager.ins.txtCountDown_Fight.text = Math.Round(countDown_Fight) + "s";
        }
    }

    private void ResetSession()
    {
        if (finishSeesion || finishDaySession)
        {
            SetUpData();
            MainGame.instance.CatCharacter.ResetData();
            MainGame.instance.DogCharacter.ResetData();
            UIManager.ins.ShowFightEff();
        }
    }

    private void NextDaySessionTime()
    {
        countDown_NextDaySessionTime -= Time.deltaTime;
        if (countDown_NextDaySessionTime <= 0)
        {
            ResetDaySession();
        }
        else
        {
            UIManager.ins.txtNextDaySessionTime.text = Math.Round(countDown_NextDaySessionTime) + "s";
        }
    }

    private void ResetDaySession()
    {
        catWins = dogWins = 0;
        MainGame.instance.ResetTotal();
        HandleStartAgainSeesion();
    }

    private void HandleStartAgainSeesion()
    {
        UIManager.ins.ActiveUI(UIManager.ins.fightingUI);
        ResetSession();
    }

    private void HandleFinishedSeesion()
    {
        finishSeesion = true;
        CheckWinner();
        UIManager.ins.ShowFinishSessionUI();
    }

    private void HandleFinishDay()
    {
        SoundManager.ins.PlaySound(SoundManager.ins.finishSound);
        UIManager.ins.ShowFinishDaySessionUI();
    }

    private void CheckWinner()
    {
        SoundManager.ins.PlaySound(SoundManager.ins.finishSound);
        if (MainGame.instance.CatCharacter.GetComponent<Character>().enabled)
        {
            UIManager.ins.txtWinner.text = "CAT WINNER!";
            catWins++;
        }
        else
        {
            UIManager.ins.txtWinner.text = "DOG WINNER!";
            dogWins++;
        } 
    }

    private void OnDestroy() => OnFinishSeesion -= HandleFinishedSeesion;
}

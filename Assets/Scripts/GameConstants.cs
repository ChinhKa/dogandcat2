public class GameConstants
{

}

public enum StateAnimation
{
    Idle,
    Attack,
    Run,
    Win,
    Hurt,
    Die
}

public enum IDCharacter
{
    dog = 0,
    cat = 1
}

public enum Key
{
    RoomID
}
﻿using TMPro;
using UnityEngine;
using DG.Tweening;

public class Character : MonoBehaviour //Lớp cơ sở của chó và mèo
{
    [Header("REFERANCE:")]
    [SerializeField] protected IDCharacter ID;
    [SerializeField] protected CharacterAnim anim;

    [Space]
    [Header("BASE INDEX:")] 
    protected int moveSpeed;
    protected int attackSpeed;
    protected int votes;
    [SerializeField] protected int forcePush = 2000;

    [Space]
    [Header("TXT:")]
    [SerializeField] protected TextMeshProUGUI atkText;
    [SerializeField] protected TextMeshProUGUI HPText;
    [SerializeField] protected TextMeshProUGUI winsText;
    [SerializeField] protected TextMeshProUGUI scoreText;
    [SerializeField] protected TextMeshProUGUI votesText;
    [SerializeField] protected TextMeshProUGUI scoreTotalText;

    [Space]
    [Header("EFFECT:")]
    [SerializeField] private ParticleSystem hitEff;

    [Space]
    [Header("SOUND:")]
    public AudioClip attackSound;

    [Space]
    [Header("OTHER:")]
    protected float countDownAttack;
    protected Transform target;
    protected Vector3 oldPos;
    //================
    protected CharacterInfo characterInfo;
    protected float HP;
    protected float maxHP;
    protected float attack;

    public virtual void Start()
    {
        oldPos = transform.position;
        LoadData();
        ResetData();
    }

    public virtual void LoadData()
    {
        characterInfo = DataManager.ins.data.characterDB.listCharacter.Find(c => c.ID == ID);
        moveSpeed = characterInfo.moveSpeed;
        attackSpeed = characterInfo.attackSpeed;
        attackSound = characterInfo.attackSound;
        ShowWins();
    }

    public virtual void Update()
    {
        if (GameManager.ins.startSeesion && GameManager.ins.isLoaded)
        {
            if (!GameManager.ins.finishSeesion && !GameManager.ins.finishDaySession)
            {
                if (Vector3.Distance(transform.position, target.position) <= 2.5f) //AttackDistance = 2.5f
                {
                    anim.TriggerAnim(StateAnimation.Idle.ToString());
                    if (countDownAttack <= 0 && GameManager.ins.enableAttackAll)
                    {
                        Attack();
                        countDownAttack = attackSpeed;
                    }
                    else
                    {
                        countDownAttack -= Time.deltaTime;
                    }
                }
                else
                {
                    Move();
                }
            }
            else
            {
                if (this.enabled)
                {
                    Won();
                }
            }
        }
    }

    #region Attack
    public virtual void Move()
    {
        anim.TriggerAnim(StateAnimation.Run.ToString());
        transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
    }

    public virtual void Attack()
    {
        anim.TriggerAnim(StateAnimation.Attack.ToString());
    }

    public virtual CharacterAnim GetCharacterAnim() => anim;
    public virtual void UpdateScore(int score)
    {
        scoreText.text = score.ToString();
    }
    public virtual void UpdateScoreTotal(int score)
    {
        scoreTotalText.text = score.ToString();
    }

    public virtual void Die()
    {
        GameManager.ins.finishSeesion = true;
        GameManager.ins.OnFinishSeesion?.Invoke();
        anim.TriggerAnim(StateAnimation.Idle.ToString());
        this.enabled = false;
    }

    public virtual void Won()
    {
        anim.TriggerAnim(StateAnimation.Win.ToString());
        Invoke("Rotate", 1f);
        Time.timeScale = 1;
        this.enabled = false;
    }
    #endregion    

    public virtual void ResetData()
    {
        votes = 0;
        votesText.text = "0";
        scoreText.text = "0";
        this.enabled = true;
        ShowWins();
        anim.ResetAnim();
        transform.position = oldPos;
        transform.rotation = Quaternion.Euler(Vector3.zero);
    }

    public virtual void IncreaseVotes()
    {
        votes++;
        votesText.text = votes.ToString();
    }

    public virtual void ShowWins()
    {

    }
    public virtual void Push()
    {
        if (anim.GetPushReady())
        {
            GameManager.ins.enableAttackAll = false;
            target.GetComponent<Character>().BePushBack();
            anim.SetPushReady(false);
        }        
    }

    public virtual void BePushBack()
    {
        anim.TriggerAnim(StateAnimation.Hurt.ToString());
    }
    public virtual void Rotate()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("RedZone"))
        {
            GameManager.ins.finishSeesion = true;
            GameManager.ins.OnFinishSeesion?.Invoke();
            this.enabled = false;
        }
    }

    private void OnDrawGizmos()
    {
        if (target != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, target.position);
        }
    }
}

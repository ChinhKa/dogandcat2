﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager ins;

    [Header("LOGIN:")]
    public string login_URL;
    public TextMeshProUGUI txtUserName_Login;
    public TextMeshProUGUI txtPassword_Login;

    [Header("REGISTER:")]
    public string register_URL;
    public TextMeshProUGUI txtUserName_Regis;
    public TextMeshProUGUI txtFullName_Regis;
    public TextMeshProUGUI txtPassword_Regis;

    private void Awake()
    {
        ins = this;
    }

    private void Update()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            UIMain.ins.ShowMess("No Internet");
        }
    }

    #region
    public void Login() {
        if (!string.IsNullOrEmpty(txtUserName_Login.text) || !string.IsNullOrEmpty(txtPassword_Login.text))
        {
            UIMain.ins.ShowMess("You need to fill in all information!");
        }
        else
        {
            StartCoroutine(LoginProcessing());
        }
    } 

    public IEnumerator LoginProcessing()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", txtUserName_Login.text);
        string hashedPassword = MD5Hasher.MD5Hash(txtPassword_Login.text);
        form.AddField("password", hashedPassword);
        UnityWebRequest rq = UnityWebRequest.Post(login_URL, form);

        yield return rq.SendWebRequest();
        if (rq.result == UnityWebRequest.Result.Success)
        {
            Debug.Log("Request Success!");
            UIMain.ins.ShowRoom();
        }
        else
        {
            UIMain.ins.ShowMess(rq.downloadHandler.text);
        }
    }
    #endregion

    #region Register
    public void Register() {
        if (!string.IsNullOrEmpty(txtUserName_Regis.text) || !string.IsNullOrEmpty(txtPassword_Regis.text) || !string.IsNullOrEmpty(txtFullName_Regis.text))
        {
            UIMain.ins.ShowMess("You need to fill in all information!");
        }
        else
        {
            StartCoroutine(RegisterProcessing()); 
        }
    }  

    public IEnumerator RegisterProcessing()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", txtUserName_Regis.text);
        string hashedPassword = MD5Hasher.MD5Hash(txtPassword_Regis.text);
        form.AddField("password", hashedPassword);
        form.AddField("fullName", txtFullName_Regis.text);
        UnityWebRequest rq = UnityWebRequest.Post(login_URL, form);

        yield return rq.SendWebRequest();
        if (rq.result == UnityWebRequest.Result.Success)
        {
            Debug.Log("Request Success!");
            Debug.Log("Response: " + rq.downloadHandler.text);
        }
    }
    #endregion  
}